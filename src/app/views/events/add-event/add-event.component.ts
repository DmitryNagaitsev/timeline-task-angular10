import { Router } from '@angular/router';
import {
  Component,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentRef,
  ViewChild,
  ChangeDetectionStrategy
} from '@angular/core';

import { EventTypes, Event } from './../models';
import { EventTypeNamePipe } from '../services/event-types.pipe';
import { resolveFormComponentType } from './add-events.factory';
import { IFormComponent } from './form-component.interface';
import { EventStorageService } from '../services/event-storage.service';

@Component({
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss'],
  providers: [EventTypeNamePipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddEventComponent {
  public selectedValue: EventTypes | null;
  public EventTypes = EventTypes;
  private componentRef: ComponentRef<any>;

  @ViewChild('container', { read: ViewContainerRef }) vcr: any;

  public constructor(
    private readonly router: Router,
    private componentFactoryResolver: ComponentFactoryResolver,
    private readonly eventStorage: EventStorageService
  ) {}

  public get eventTypes(): Array<string> {
    const keys = Object.values(this.EventTypes) as string[];
    return keys.slice(keys.length / 2);
  }

  public onSelected(type: EventTypes): void {
    // TODO: переделать на модалки
    if (this.componentRef) {
      this.componentRef.destroy();
    }

    const eventContentComponent = resolveFormComponentType(Number(type));
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      eventContentComponent
    );
    this.componentRef = this.vcr.createComponent(componentFactory);
    (this.componentRef.instance as IFormComponent<Event>).onSubmit.subscribe(
      formValue => {
        this.eventStorage.addEvent(formValue);
        this.router.navigate(['/events/registry']);
      }
    );
  }
}
