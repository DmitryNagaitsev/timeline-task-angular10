import { Component, ChangeDetectionStrategy } from '@angular/core';
import { News } from '../../../models';
import { EventFormComponent } from '../event-form.component';

@Component({
  templateUrl: './news-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewsFormComponent extends EventFormComponent {
  public model = new News();

  protected getFormValue(): News {
    return this.model;
  }
}
