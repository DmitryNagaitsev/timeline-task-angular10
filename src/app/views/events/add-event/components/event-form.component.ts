import { ViewChild, Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Event } from '../../models';
import { Subject } from 'rxjs';
import { IFormComponent } from '../form-component.interface';

@Component({ template: '' })
export abstract class EventFormComponent implements IFormComponent<Event> {
  public submitted = false;
  @ViewChild('form') form: NgForm;
  private onSubmitSubject = new Subject<Event>();
  public onSubmit = this.onSubmitSubject.asObservable();

  public onFormSubmit(): void {
    this.submitted = true;
    if (this.form.valid) {
      const model = this.getFormValue();
      model.date = new Date();
      this.onSubmitSubject.next(model);
    }
  }

  protected abstract getFormValue(): Event;
}
