import { Component, ChangeDetectionStrategy } from '@angular/core';

import { FinancialTransaction } from './../../../models';
import { EventFormComponent } from '../event-form.component';

@Component({
  templateUrl: './transaction-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransactionFormComponent extends EventFormComponent {
  public model = new FinancialTransaction();

  protected getFormValue(): FinancialTransaction {
    return this.model;
  }
}
