import { Observable } from 'rxjs';

export interface IFormComponent<T> {
  onSubmit: Observable<T>;
}
