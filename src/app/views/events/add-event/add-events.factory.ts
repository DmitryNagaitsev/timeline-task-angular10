import { Type } from '@angular/core';
import { EventTypes } from '../models';
import { NewsFormComponent } from './components/news-form/news-form.component';
import { TransactionFormComponent } from './components/transaction-form/transaction-form.component';

export function resolveFormComponentType(type: EventTypes): Type<any> {
  switch (type) {
    case EventTypes.FinancialTransaction:
      return TransactionFormComponent;
    case EventTypes.News:
      return NewsFormComponent;
    default:
      throw new Error(`Компонент для типа ${type} не реализован`);
  }
}
