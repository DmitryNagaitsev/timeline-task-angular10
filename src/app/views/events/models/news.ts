import { EventTypes } from './event-types';
import { Event } from './event';

export class News extends Event {
  public header: string;
  public content: string;
  public viewed = false;

  public get type(): EventTypes {
    return EventTypes.News;
  }
}
