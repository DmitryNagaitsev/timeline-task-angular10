import {
  Component,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  ChangeDetectorRef,
  AfterViewInit,
  ChangeDetectionStrategy
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventStorageService } from './../services/event-storage.service';
import { resolveFullComponentType } from './event-display.factory';

@Component({
  templateUrl: './event-display.component.html',
  styleUrls: ['./event-display.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventDisplayComponent implements AfterViewInit {
  @ViewChild('container', { read: ViewContainerRef }) vcr: any;

  constructor(
    private readonly componentFactoryResolver: ComponentFactoryResolver,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly eventStorage: EventStorageService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngAfterViewInit(): void {
    this.route.params.subscribe(params => {
      const id = +params.id;
      const event = this.eventStorage.getById(id);
      const eventContentComponent = resolveFullComponentType(event.type);
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
        eventContentComponent
      );
      this.vcr.clear();
      const componentRef = this.vcr.createComponent(componentFactory);
      componentRef.instance.event = event;
      this.changeDetectorRef.detectChanges();
    });
  }

  public navigateToRegistry(): void {
    this.router.navigate(['/events/registry']);
  }
}
