import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { EventStorageService } from './../../../../services/event-storage.service';
import { FinancialTransaction } from '../../../../models';

@Component({
  templateUrl: './transaction-full.component.html',
  styleUrls: ['./transaction-full.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransactionFullComponent {
  @Input() public event: FinancialTransaction;

  public constructor(
    private readonly eventStorage: EventStorageService,
    private readonly router: Router
  ) {}

  public deleteEvent(): void {
    this.eventStorage.removeEvent(this.event.id);
    this.router.navigate(['/events/registry']);
  }
}
