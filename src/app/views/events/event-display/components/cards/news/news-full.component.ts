import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy
} from '@angular/core';
import { EventStorageService } from './../../../../services/event-storage.service';
import { News } from './../../../../models';

@Component({
  templateUrl: './news-full.component.html',
  styleUrls: ['./news-full.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewsFullComponent implements OnInit {
  @Input() public event: News;

  public constructor(private readonly eventStorage: EventStorageService) {}

  public ngOnInit(): void {
    this.eventStorage.markViewed(this.event.id);
  }
}
