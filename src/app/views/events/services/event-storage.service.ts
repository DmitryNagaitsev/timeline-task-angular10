import { Injectable } from '@angular/core';
import { Event, EventTypes, News, FinancialTransaction } from '../models';

/**
 * TODO: заменить на нормальную бд
 */
@Injectable()
export class EventStorageService {
  private counter = 3;
  private events: Map<number, Event> = new Map([
    [
      1,
      Object.assign(new News(), {
        id: 1,
        date: new Date(),
        header: 'Курс евро впервые с 2016 года превысил 92 рубля',
        content:
          'Курс евро в ходе торгов на Московской бирже превысил 92 рубля впервые с января 2016 года. Об этом свидетельствуют данные площадки в понедельник, 28 сентября.',
        viewed: false
      }) as Event
    ],
    [
      2,
      Object.assign(new FinancialTransaction(), {
        id: 2,
        date: new Date(),
        total: 250,
        currency: 'рублей',
        transactionFrom: 'ООО Рога и Копыта',
        description: 'денежный перевод'
      }) as Event
    ]
  ]);

  public getEvents(): Event[] {
    return Array.from(this.events.values());
  }

  public addEvent(event: Event): void {
    event.id = this.counter;
    this.events.set(this.counter, event);
    this.counter++;
  }

  public removeEvent(id: number): void {
    this.events.delete(id);
  }

  public getById(id: number): Event {
    return this.events.get(id);
  }

  public markViewed(id: number): void {
    const event = this.getById(id);
    if (!event && event.type !== EventTypes.News) {
      throw new Error('Признак прочтения доступен только для новости');
    }
    (event as News).viewed = true;
  }
}
