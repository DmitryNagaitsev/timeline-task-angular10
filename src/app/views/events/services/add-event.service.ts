import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Event } from '../models';

@Injectable()
export class AddEventService {
  private addEventSource = new Subject<Event>();

  public eventAdded = this.addEventSource.asObservable();

  publishAddEventRequest(event: Event): void {
    this.addEventSource.next(event);
  }
}
