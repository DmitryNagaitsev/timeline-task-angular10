import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventsRoutingModule } from './events-routing.module';

import { EventDisplayComponent } from './event-display';
import {
  NewsCardComponent,
  TransactionCardComponent,
  EventsRegistryComponent,
  EventCardComponent
} from './events-registry';
import { AddEventComponent } from './add-event/add-event.component';
import { EventTypeNamePipe } from './services/event-types.pipe';
import { NewsFormComponent } from './add-event/components/news-form/news-form.component';
import { TransactionFormComponent } from './add-event/components/transaction-form/transaction-form.component';
import { FormsModule } from '@angular/forms';
import { AddEventService } from './services/add-event.service';
import {
  NewsFullComponent,
  TransactionFullComponent
} from './event-display/components/cards';

const EventCardComponents = [NewsCardComponent, TransactionCardComponent];

const EventFormComponents = [NewsFormComponent, TransactionFormComponent];

const EventFullComponents = [NewsFullComponent, TransactionFullComponent];

@NgModule({
  imports: [CommonModule, FormsModule, EventsRoutingModule],
  declarations: [
    EventsRegistryComponent,
    EventDisplayComponent,
    ...EventFullComponents,
    EventCardComponent,
    ...EventCardComponents,
    AddEventComponent,
    ...EventFormComponents,
    EventTypeNamePipe
  ],
  exports: [EventsRegistryComponent, EventDisplayComponent],
  providers: [AddEventService],
  entryComponents: [
    ...EventFullComponents,
    ...EventCardComponents,
    ...EventFormComponents
  ]
})
export class EventsModule {}
