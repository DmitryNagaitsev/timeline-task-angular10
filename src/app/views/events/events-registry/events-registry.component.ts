import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Event } from '../models';
import { EventStorageService } from '../services/event-storage.service';

@Component({
  templateUrl: './events-registry.component.html',
  styleUrls: ['./events-registry.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventsRegistryComponent implements OnInit {
  public timelineEvents: Event[] = [];

  public constructor(
    private readonly router: Router,
    private readonly eventStorageService: EventStorageService
  ) {}

  public ngOnInit(): void {
    this.timelineEvents = this.eventStorageService.getEvents();
  }

  public addEvent(): void {
    this.router.navigate(['/events/create']);
  }
}
