import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { FinancialTransaction } from '../../../../models';

@Component({
  templateUrl: './transaction-card.component.html',
  styleUrls: ['./transaction-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransactionCardComponent {
  @Input() public event: FinancialTransaction;
}
