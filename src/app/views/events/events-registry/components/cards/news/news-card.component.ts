import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { News } from './../../../../models';

@Component({
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewsCardComponent {
  @Input() public event: News;
}
