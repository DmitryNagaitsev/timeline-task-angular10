import {
  Component,
  Input,
  Type,
  ComponentFactoryResolver,
  ViewChild,
  ViewContainerRef,
  AfterViewInit,
  ChangeDetectorRef
} from '@angular/core';
import { Router } from '@angular/router';
import { Event } from '../../../models';
import { resolveComponentType } from './event-card.factory';

@Component({
  selector: 'event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss']
})
export class EventCardComponent implements AfterViewInit {
  public eventContentComponent: Type<any>;

  @Input() event: Event;
  @ViewChild('container', { read: ViewContainerRef }) vcr: any;

  public constructor(
    private readonly componentFactoryResolver: ComponentFactoryResolver,
    private readonly router: Router,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngAfterViewInit(): void {
    // TODO: переделать на модалки
    const eventContentComponent = resolveComponentType(this.event.type);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      eventContentComponent
    );
    const componentRef = this.vcr.createComponent(componentFactory);
    componentRef.instance.event = this.event;
    this.changeDetectorRef.detectChanges();
  }

  public openFullInfo(): void {
    this.router.navigate(['/events/display', this.event.id]);
  }
}
